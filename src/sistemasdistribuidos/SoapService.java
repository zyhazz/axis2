package sistemasdistribuidos;

public class SoapService {
	public float Soma(float num1, float num2) {
		return num1 + num2;
	}
	public float Subtracao(float num1, float num2) {
		return num1 - num2;
	}
	public float Multiplicacao(float num1, float num2) {
		return num1 * num2;
	}
	public float Divisao(float num1, float num2) {
		return num1/num2;
	}
	public float Media(float num1, float num2, float num3) {
		return (num1 + num2 + num3)/3;
	}
	public float Raiz(float num) {
		return (float) Math.sqrt(num);
	}
	public float Quadrado(float num) {
		return num*num;
	}
	public float Cubico(float num) {
		return num*num*num;
	}
	public float Fatorial(int num) {
		if(num <= 1) {
			return 1;
		}else {
			return num * Fatorial(num - 1);
		}
	}
	public int Somatorio(int num) {
		if(num <= 1) {
			return 1;
		}else {
			return num + Somatorio(num - 1);
		}
	}
	public int Antecessor(int num) {
		return num - 1;
	}
	public int Sucessor(int num) {
		return num + 1;
	}
	public int Menor(int num1, int num2) {
		if(num1 < num2) {
			return num1;
		}else {
			return num2;
		}
	}
	public int Maior(int num1, int num2) {
		if(num1 > num2) {
			return num1;
		}else {
			return num2;
		}
	}
	public int Fibonacci(int num) {
		if(num <= 0) {
			return 0;
		}
		if(num == 1) {
			return 1;
		}
		return Fibonacci(num - 1) + Fibonacci(num - 2);
	}
}
